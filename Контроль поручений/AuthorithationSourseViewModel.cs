﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OrdersControl
{
    class AuthorithationSourseViewModel : BaseInpc
    {
        private string _fio;
        public string FIO { get => _fio; set => Set(ref _fio, value); }
        private string _password;
        public string Password { get => _password; set => Set(ref _password, value); }
        private bool _admin;
        private int localpassword = 1861749232;
        public bool Admin { get => _admin; set => Set(ref _admin, value); }
        private bool _checked;
        public bool Checked { get => _checked; set => Set(ref _checked, value); }
        private RelayCommand _authorithationCommand;
        public RelayCommand AuthorithationCommand
        {
            get
            {
                return _authorithationCommand ??
                    (_authorithationCommand = new RelayCommand(obj =>
                    {
                        Authorithation();
                    }));
            }
        }

        private void Authorithation()
        {
            if (Admin)
            {
                if (Password.GetHashCode() == localpassword && FIO != "")
                {
                    MainWindow main = new MainWindow(FIO, !Admin);
                    main.Show();
                    App.Current.MainWindow = main;
                    if (Checked)
                    {
                        List<string> AuthInf = new List<string>();
                        AuthInf.Add(FIO);                       
                        AuthInf.Add(Password);
                        File.WriteAllLines("AuthorithationFile.txt", AuthInf);
                    }
                    foreach (Window it in App.Current.Windows)
                    {
                        if (it is Authorithation)
                        {
                            it.Close();
                        }
                    }
                }
                else if (Password.GetHashCode() != localpassword)
                {
                    MessageBox.Show("Неверный пароль!", "Авторизация", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show("Вы забыфли ввести своё ФИО!", "Авторизация", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                if (FIO != "")
                {
                    MainWindow main = new MainWindow(FIO, !Admin);
                    main.Show();
                    App.Current.MainWindow = main;
                    if (Checked)
                    {
                        List<string> AuthInf = new List<string>();
                        AuthInf.Add(FIO);
                        File.WriteAllLines("AuthorithationFile.txt", AuthInf);
                    }
                    foreach (Window it in App.Current.Windows)
                    {
                        if (it is Authorithation)
                        {
                            it.Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Вы забыфли ввести своё ФИО!", "Авторизация", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private RelayCommand _goEdit1Command;
        public RelayCommand GoEdit1Command
        {
            get
            {
                return _goEdit1Command ??
                    (_goEdit1Command = new RelayCommand(obj =>
                    {
                        PasswordBox passwordBox = (PasswordBox)obj;
                        if (Password == "" && Admin)
                        {
                            passwordBox.Focus();
                        }
                        else
                        {
                            passwordBox.Focus();
                            Authorithation();
                        }
                    }));
            }
        }
        private RelayCommand _goEdit2Command;
        public RelayCommand GoEdit2Command
        {
            get
            {
                return _goEdit2Command ??
                    (_goEdit2Command = new RelayCommand(obj =>
                    {
                        TextBox passwordBox = (TextBox)obj;
                        if (FIO == "")
                        {
                            passwordBox.Focus();
                        }
                        else
                        {
                            Authorithation();
                        }
                    }));
            }
        }

        private RelayCommand _passwordChangedCommand;
        public RelayCommand PasswordChangedCommand
        {
            get
            {
                return _passwordChangedCommand ??
                    (_passwordChangedCommand = new RelayCommand(obj =>
                    {
                        PasswordBox passwordBox = (PasswordBox)obj;
                        Password = passwordBox.Password;
                    }));
            }
        }
        private RelayCommand _textChangedCommand;
        public RelayCommand TextChangedCommand
        {
            get
            {
                return _textChangedCommand ??
                    (_textChangedCommand = new RelayCommand(obj =>
                    {
                        FIO = obj.ToString();
                    }));
            }
        }

        private RelayCommand _giveRootsCommand;

        public AuthorithationSourseViewModel()
        {
            List<string> AuthorInf = File.ReadAllLines("AuthorithationFile.txt").ToList();
            if (AuthorInf.Count == 1)
            {
                FIO = AuthorInf[0];
                Checked = true;
            }
            else if (AuthorInf.Count == 2)
            {
                Admin = true;
                Checked = true;
                FIO = AuthorInf[0];
                Password = AuthorInf[1];
            }
            else
            {
                Password = "";
                FIO = "";
            }
        }

        public RelayCommand GiveRootsCommand
        {
            get
            {
                return _giveRootsCommand ??
                    (_giveRootsCommand = new RelayCommand(obj =>
                    {
                        Admin = true;
                    }));
            }
        }
    }
}
