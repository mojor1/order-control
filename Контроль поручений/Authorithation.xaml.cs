﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OrdersControl
{
    /// <summary>
    /// Логика взаимодействия для Authorithation.xaml
    /// </summary>
    public partial class Authorithation : Window
    {
        public Authorithation()
        {
            InitializeComponent();
            try
            {
                PasswordText.Password = File.ReadAllLines("AuthorithationFile.txt").ToList()[1];
            }
            catch
            {

            }
        }
    }
}
