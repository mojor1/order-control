﻿using System;
using System.Drawing.Printing;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Collections.Generic;
using Microsoft.Win32;
using Notifications.Wpf;

namespace OrdersControl
{
    public class OrdersSourceViewModel : BaseInpc
    {
        string Name { get; set; }
        private BindingList<Order> _orders;
        private string _ordersFile;
        private DateTime lastAccessOrders;
        private DateTime lastAccessType;
        private string FIO;
        bool firstTime;
        public string OrdersFile 
        {
            get=> _ordersFile; 
            set
            {
                Set(ref _ordersFile, value);
                Orders = System.Text.Json.JsonSerializer.Deserialize<BindingList<Order>>(File.ReadAllText(OrdersFile));
                if (IsReadOnly)
                {
                    Orders = new BindingList<Order>(Orders.Where(it => it.PerformerName == FIO).ToList());
                }
                firstTime = true;
                SelectedIndex = 0;
                AllOrders = null;
                ChangeTypesInOrders();
                DirectoryInfo directory = new DirectoryInfo(OrdersFile);
                lastAccessOrders = directory.LastAccessTime;
                Orders.ListChanged += Orders_ListChanged;
            }
        }
        private string _typeFile;
        public string TypeFile
        {
            get => _typeFile;
            set
            {
                Set(ref _typeFile, value);
                TypeOrders = System.Text.Json.JsonSerializer.Deserialize<BindingList<TypeOrder>>(File.ReadAllText(TypeFile));
                ChangeTypesInOrders();
                DirectoryInfo directory = new DirectoryInfo(TypeFile);
                lastAccessType = directory.LastAccessTime;
                TypeOrders.ListChanged += TypeOrders_ListChanged;
            }
        }
        public BindingList<Order> Orders { get =>_orders; set => Set(ref _orders, value); }
        private object _selectedOrder;
        public object SelectedOrder { get => _selectedOrder; set => Set(ref _selectedOrder, value); }
        private BindingList<TypeOrder> _typeOrders;
        private bool _isReadOnly;
        public bool IsReadOnly { get => _isReadOnly; set => Set(ref _isReadOnly, value); }
        public BindingList<TypeOrder> TypeOrders { get => _typeOrders; set => Set(ref _typeOrders, value); }
        private object locker = new();
        public object lockerOrders = new();

        public OrdersSourceViewModel(string fio, bool isReadOnly)
        {
            FIO = fio;
            IsReadOnly = isReadOnly;
            var files = File.ReadAllLines("1.txt");
            try
            {
                OrdersFile = files[0];
                Orders.AllowEdit = true;
                Orders.AllowNew = true;
                Orders.AllowRemove = true;
                Orders.RaiseListChangedEvents = true;
            }
            catch
            {
                OrdersFile = "1.json";
                Orders.AllowEdit = true;
                Orders.AllowNew = true;
                Orders.AllowRemove = true;
                Orders.RaiseListChangedEvents = true;
            }
            try
            {
                TypeFile = files[1];
                TypeOrders.ListChanged += TypeOrders_ListChanged;
            }
            catch
            {
                TypeFile = "type.json";
                TypeOrders.ListChanged += TypeOrders_ListChanged;
            }
            WriteFirstFile();
            Thread thread = new Thread(() => 
            {
                    while (true)
                    {
                        MakeNotification();
                        Thread.Sleep(1000000);
                    }
            });
            thread.IsBackground = true;
            thread.Start();
            Thread thread2 = new Thread(() =>
            {
                while (true)
                {
                    try
                    {
                        if (!changing)
                        {
                            DirectoryInfo directoryOrders = new DirectoryInfo(OrdersFile);
                            DirectoryInfo directoryType = new DirectoryInfo(TypeFile);
                            App.Current.Dispatcher.Invoke(() =>
                            {
                                lock (lockerOrders)
                                {
                                    if (lastAccessOrders < directoryOrders.LastWriteTime)
                                    {                                      
                                            Orders = JsonSerializer.Deserialize<BindingList<Order>>(File.ReadAllText(OrdersFile));
                                        if (IsReadOnly)
                                        {
                                            Orders = new BindingList<Order>(Orders.Where(it => it.PerformerName == FIO).ToList());
                                        }
                                            SelectedIndex = 0;
                                            AllOrders = null;
                                            lastAccessOrders = directoryOrders.LastWriteTime;
                                            Orders.ListChanged += Orders_ListChanged;                            
                                    }
                                }
                            });
                            if (lastAccessType < directoryType.LastWriteTime)
                            {
                                App.Current.Dispatcher.Invoke(() =>
                                {
                                    lock (locker)
                                    {
                                        TypeOrders = JsonSerializer.Deserialize<BindingList<TypeOrder>>(File.ReadAllText(TypeFile));
                                        lastAccessType = directoryType.LastWriteTime;
                                        TypeOrders.ListChanged += TypeOrders_ListChanged;
                                    }
                                });
                            }
                        }
                        Thread.Sleep(1000);
                    }
                    catch
                    { 
                    }
                }
            });
            thread2.IsBackground = true;
            thread2.Start();
        }
        private bool changing = false;
        public void MakeNotification()
        {
            var notificationManager = new NotificationManager();
            if (AllOrders == null)
            {
                Notification(notificationManager, Orders);
            }
            else
            {
                Notification(notificationManager, AllOrders);
            }
        }

        private void Notification(NotificationManager notificationManager, BindingList<Order> orders)
        {
            notificationManager.Show(new NotificationContent
            {
                Title = "Отправленные на отметку об исполнении",
                Message = $"{orders.Count(it => !it.IsFinished && it.IsInRequest)} поручений ждут отметку об исполнении",
                Type = NotificationType.Information                
            }, expirationTime: TimeSpan.FromSeconds(30));
            notificationManager.Show(new NotificationContent
            {
                Title = "Просрочка",
                Message = $"{orders.Count(it => !it.IsFinished && it.Date_end < DateTime.Now)} поручений просрочены",
                Type = NotificationType.Error
            }, expirationTime: TimeSpan.FromSeconds(30));
            notificationManager.Show(new NotificationContent
            {
                Title = "Истекает срок поручения",
                Message = $"У {orders.Count(it => !it.IsFinished && (DateTime.Now - it.Date_end).Days <= it.TypeOrder.CountDay && it.Date_end > DateTime.Now)} поручений срок подходит к концу",
                Type = NotificationType.Warning
            }, expirationTime: TimeSpan.FromSeconds(30));
        }

        private void ChangeTypesInOrders()
        {
            if (Orders != null && TypeOrders !=null)
            {
                lock (lockerOrders)
                {
                    for (int i = 0; i < Orders.Count; i++)
                    {
                        if (!TypeOrders.Any(it => it == Orders[i].TypeOrder))
                            Orders[i].TypeOrder = TypeOrders[0];
                    }
                }
            }
        }

        private RelayCommand _openFileCommand;
        public RelayCommand OpenFileCommand
        {
            get
            {
                return _openFileCommand ??
                    (_openFileCommand = new RelayCommand(obj =>
                    {
                        TextBlock textBlock = (TextBlock)obj;
                        OpenFileDialog openFileDialog = new OpenFileDialog();
                        openFileDialog.Filter = "Json files (*.json)|*.json";
                        if (openFileDialog.ShowDialog() == true)
                        {
                            textBlock.Text = openFileDialog.FileName;
                        }
                        WriteFirstFile();

                    }));
            }
        }

        private void WriteFirstFile()
        {
            var write = new List<string>();
            write.Add(OrdersFile);
            write.Add(TypeFile);
            File.WriteAllLines("1.txt", write);
        }

        private RelayCommand _onClosingCommand;
        public RelayCommand OnClosingCommand
        {
            get
            {
                return _onClosingCommand ??
                    (_onClosingCommand = new RelayCommand(obj =>
                    {
                        if (AllOrders != null)
                        {
                            Orders = AllOrders;
                            SaveOrders(1);
                        }
                    }));
            }
        }

        bool changed;
        private void TypeOrders_ListChanged(object sender, ListChangedEventArgs e)
        {
            lock (lockerOrders)
            {
                for (int i = 0; i < Orders.Count; i++)
                {
                    if (Orders[i].TypeOrder!=null && Orders[i].TypeOrder.BackgroundOrder == TypeOrders[e.NewIndex].BackgroundOrder)
                        Orders[i].TypeOrder = TypeOrders[0];
                }
            }
            Thread thread = new Thread(() => Application.Current.Dispatcher.Invoke(() =>
            {
                changing = true;
                lock (locker)
                {
                    File.WriteAllText(TypeFile, JsonSerializer.Serialize<BindingList<TypeOrder>>(TypeOrders));
                    DirectoryInfo directoryType = new DirectoryInfo(TypeFile);
                    lastAccessOrders = directoryType.LastWriteTime;
                }
                changing = false;
            }));
            thread.Start();
        }

        private void Orders_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (!firstTime)
            {
                SaveOrders(e.NewIndex);
            }
            firstTime = false;
        }

        private void SaveOrders(int index)
        {
            Thread thread = new Thread(() => Application.Current.Dispatcher.Invoke(() =>
            {
                changing = true;
                lock (lockerOrders)
                {
                    if (IsReadOnly)
                    {
                        var localAllOrders = JsonSerializer.Deserialize<BindingList<Order>>(File.ReadAllText(OrdersFile));
                        localAllOrders[localAllOrders.IndexOf(localAllOrders.Where(it => it.PerformerName == Orders[index].PerformerName && it.NumbDoc == Orders[index].NumbDoc).ToList()[0])].IsInRequest = true;
                    }
                    else 
                    {
                        File.WriteAllText(OrdersFile, JsonSerializer.Serialize<BindingList<Order>>(Orders));                       
                    }
                    DirectoryInfo directoryOrders = new DirectoryInfo(OrdersFile);
                    lastAccessOrders = directoryOrders.LastWriteTime;
                }
                changing = false;
            }));
            thread.Start();
        }

        private RelayCommand buttonClickCommand;
        private int counter = 0;
        public RelayCommand ButtonClickCommand
        {
            get
            {
                return buttonClickCommand ??
                    (buttonClickCommand = new RelayCommand(obj =>
                    {
                        if (counterRightPanel % 2 == 1)
                            OpenOrCloseRightPanel(RightPanel);
                        OpenOrCloseLeftPanel(obj);
                    }));
            }
        }
        private RelayCommand _selectionTypeChengedCommand;
        public RelayCommand SelectionTypeChengedCommand
        {
            get
            {
                return _selectionTypeChengedCommand ??
                    (_selectionTypeChengedCommand = new RelayCommand(obj =>
                    {
                        if (SelectedIndexTypeOrder > -1 && _selectedItems != null && _selectedItems.Count > 0)
                            lock (lockerOrders)
                            {
                                for (int i = 0; i < _selectedItems.Count; i++)
                                {
                                    Orders[Orders.IndexOf(_selectedItems[i])].TypeOrder = TypeOrders[SelectedIndexTypeOrder];
                                }
                            }
                    }));
            }
        }
        private RelayCommand _addTypeOrderCommand;
        public RelayCommand AddTypeOrderCommand
        {
            get
            {
                return _addTypeOrderCommand ??
                    (_addTypeOrderCommand = new RelayCommand(obj =>
                    {
                        SelectedIndexTypeOrder = -1;
                        TypeOrders.Add(new TypeOrder());
                    }));
            }
        }
        ListBox LeftPanel;

        private void OpenOrCloseLeftPanel(object obj)
        {
            counter++;
            LeftPanel = obj as ListBox;
            var bind = new Binding();
            if (counter % 2 == 1)
            {

                DoubleAnimation LeftPanelAnimation = new DoubleAnimation();
                LeftPanelAnimation.From = LeftPanel.ActualWidth;
                LeftPanelAnimation.To = LeftPanel.MaxWidth;
                LeftPanelAnimation.Duration = TimeSpan.FromSeconds(5);
                LeftPanel.BeginAnimation(ListBox.WidthProperty, LeftPanelAnimation);
                bind.Source = LeftPanel;
                bind.Path = new PropertyPath("MaxWidth");
                new Task(() => { Thread.Sleep(5000); Application.Current.Dispatcher.Invoke(new Action(() => LeftPanel.SetBinding(ListBox.MinWidthProperty, bind))); }).Start();
            }
            else
            {
                BindingOperations.ClearBinding(LeftPanel, ListBox.MinWidthProperty);
                DoubleAnimation LeftPanelAnimation = new DoubleAnimation();
                LeftPanelAnimation.From = LeftPanel.ActualWidth;
                LeftPanelAnimation.To = 0;
                LeftPanelAnimation.Duration = TimeSpan.FromSeconds(5);
                LeftPanel.BeginAnimation(ListBox.WidthProperty, LeftPanelAnimation);
            }
        }

        private RelayCommand buttonClickRightPanelCommand;
        private int counterRightPanel = 0;
        public RelayCommand ButtonClickRightPanelCommand
        {
            get
            {
                return buttonClickRightPanelCommand ??
                    (buttonClickRightPanelCommand = new RelayCommand(obj =>
                    {
                        if (counter % 2 == 1)
                            OpenOrCloseLeftPanel(LeftPanel);
                        OpenOrCloseRightPanel(obj);
                    }));
            }
        }

        private RelayCommand _buttonClickChooseCommand;
        private int counterChoose = 0;
        public RelayCommand ButtonClickChooseCommand
        {
            get
            {
                return _buttonClickChooseCommand ??
                    (_buttonClickChooseCommand = new RelayCommand(obj =>
                    {
                        OpenOrCloseChoosePanel(obj);
                    }));
            }
        }
        StackPanel RightPanel;
        private void OpenOrCloseRightPanel(object obj)
        {
            counterRightPanel++;
            RightPanel = obj as StackPanel;
            var bind = new Binding();
            if (counterRightPanel % 2 == 1)
            {

                DoubleAnimation LeftPanelAnimation = new DoubleAnimation();
                LeftPanelAnimation.From = RightPanel.ActualWidth;
                LeftPanelAnimation.To = RightPanel.MaxWidth;
                LeftPanelAnimation.Duration = TimeSpan.FromSeconds(5);
                RightPanel.BeginAnimation(StackPanel.WidthProperty, LeftPanelAnimation);
                bind.Source = RightPanel;
                bind.Path = new PropertyPath("MaxWidth");
                new Task(() => { Thread.Sleep(5000); Application.Current.Dispatcher.Invoke(new Action(() => RightPanel.SetBinding(StackPanel.MinWidthProperty, bind))); }).Start();
            }
            else
            {
                BindingOperations.ClearBinding(RightPanel, StackPanel.MinWidthProperty);
                DoubleAnimation LeftPanelAnimation = new DoubleAnimation();
                LeftPanelAnimation.From = RightPanel.ActualWidth;
                LeftPanelAnimation.To = 0;
                LeftPanelAnimation.Duration = TimeSpan.FromSeconds(5);
                RightPanel.BeginAnimation(StackPanel.WidthProperty, LeftPanelAnimation);
            }
        }

        private void OpenOrCloseChoosePanel(object obj)
        {
            counterChoose++;
            StackPanel ChoosePanel = obj as StackPanel;
            if (counterChoose % 2 == 1)
            {

                DoubleAnimation LeftPanelAnimation = new DoubleAnimation();
                LeftPanelAnimation.From = ChoosePanel.ActualHeight;
                LeftPanelAnimation.To = ChoosePanel.MaxHeight;
                ChoosePanel.BeginAnimation(StackPanel.HeightProperty, LeftPanelAnimation);
            }
            else
            {
                DoubleAnimation LeftPanelAnimation = new DoubleAnimation();
                LeftPanelAnimation.From = ChoosePanel.ActualHeight;
                LeftPanelAnimation.To = 0;
                LeftPanelAnimation.Duration = TimeSpan.FromSeconds(5);
                ChoosePanel.BeginAnimation(StackPanel.HeightProperty, LeftPanelAnimation);
            }
        } 

        private BindingList<Order> _allOrders;
        public BindingList<Order> AllOrders { get => _allOrders; set => Set(ref _allOrders, value); }
        private RelayCommand _allOrdersCommand;
        public RelayCommand AllOrdersCommand
        {
            get
            {
                return _allOrdersCommand ??
                    (_allOrdersCommand = new RelayCommand(obj =>
                    {
                        AllOrdersFunc();
                        if (changed)
                        {
                            SaveOrders(1);
                        }
                    }));
            }
        }

        private void AllOrdersFunc()
        {
            SelectedIndex = 0;
            if (AllOrders is not null)
            {
                Orders = AllOrders;
                Orders.AllowEdit = true;
                Orders.AllowNew = true;
                Orders.AllowRemove = true;
                Orders.RaiseListChangedEvents = true;
                AllOrders = null;
            }
        }

        private RelayCommand _printCommand;
        public RelayCommand PrintCommand
        {
            get
            {
                return _printCommand ??
                    (_printCommand = new RelayCommand(obj =>
                    {
                        ListBox listBox = obj as ListBox;
                        CommandPrintAllExecute(listBox);
                    }));
            }
        }
        private void CommandPrintAllExecute(ListBox MainListBox)
        {
            Border bd = MainListBox.Template.FindName("Bd", MainListBox) as Border;
            ItemsPresenter itp = (bd.Child as ScrollViewer).Content as ItemsPresenter;
            RenderTargetBitmap rbmp = new RenderTargetBitmap((int)itp.ActualWidth, (int)itp.ActualHeight, 96, 96, PixelFormats.Default);
            rbmp.Render(itp);
            MemoryStream stream = new MemoryStream();
            BitmapEncoder encoder = new BmpBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(rbmp));
            encoder.Save(stream);
            Bitmap bitmap = new Bitmap(stream);
            PrintDocument doc = new PrintDocument();
            int count = 0;
            doc.PrintPage += (ar, arg) =>
            {
                arg.Graphics.DrawImage(bitmap, 0, (0 - count) * arg.PageSettings.PaperSize.Height);
                if ((count+1) * arg.PageBounds.Height < bitmap.Height)
                    arg.HasMorePages = true;
                else
                    arg.HasMorePages = false;
                count++;
            };
            PrintDialog printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == true)
            {
                doc.Print();
            }
        }
        private RelayCommand _completedOrdersCommand;
        public RelayCommand CompletedOrdersCommand
        {
            get
            {
                return _completedOrdersCommand ??
                    (_completedOrdersCommand = new RelayCommand(obj =>
                    {
                        SelectedIndex = 1;
                        if (AllOrders is null)
                        {
                            AllOrders = Orders;
                            AllOrders.AllowEdit = true;
                            AllOrders.AllowNew = true;
                            AllOrders.AllowRemove = true;
                            AllOrders.RaiseListChangedEvents = true;
                        }
                        if (AllOrders is null)
                        {
                            Orders = new BindingList<Order>(Orders.Where(el => el.IsFinished).ToList());
                            Orders.AllowEdit = true;
                            Orders.AllowNew = true;
                            Orders.AllowRemove = true;
                            Orders.RaiseListChangedEvents = true;
                        }
                        else
                        {
                            Orders = new BindingList<Order>(AllOrders.Where(el => el.IsFinished).ToList());
                            Orders.AllowEdit = true;
                            Orders.AllowNew = true;
                            Orders.AllowRemove = true;
                            Orders.RaiseListChangedEvents = true;
                        }
                    }));
            }
        }
        
        private RelayCommand _executableOrdersCommand;
        public RelayCommand ExecutableOrdersCommand
        {
            get
            {
                return _executableOrdersCommand ??
                    (_executableOrdersCommand = new RelayCommand(obj =>
                    {
                        SelectedIndex = 2;
                        if (AllOrders is null)
                        {
                            AllOrders = Orders;
                            AllOrders.AllowEdit = true;
                            AllOrders.AllowNew = true;
                            AllOrders.AllowRemove = true;
                            AllOrders.RaiseListChangedEvents = true;
                        }
                        if (AllOrders is null)
                        {
                            Orders = new BindingList<Order>(Orders.Where(el => !el.IsFinished && !el.IsInRequest).ToList());
                            Orders.AllowEdit = true;
                            Orders.AllowNew = true;
                            Orders.AllowRemove = true;
                            Orders.RaiseListChangedEvents = true;
                        }
                        else
                        {
                            Orders = new BindingList<Order>(AllOrders.Where(el => !el.IsFinished && !el.IsInRequest).ToList());
                            Orders.AllowEdit = true;
                            Orders.AllowNew = true;
                            Orders.AllowRemove = true;
                            Orders.RaiseListChangedEvents = true;
                        }                      
                    }));
            }
        }
        private RelayCommand _requestedOrdersCommand;
        public RelayCommand RequestedOrdersCommand
        {
            get
            {
                return _requestedOrdersCommand ??
                    (_requestedOrdersCommand = new RelayCommand(obj =>
                    {
                        SelectedIndex = 3;
                        if (AllOrders is null)
                        {
                            AllOrders = Orders;
                            AllOrders.AllowEdit = true;
                            AllOrders.AllowNew = true;
                            AllOrders.AllowRemove = true;
                            AllOrders.RaiseListChangedEvents = true;
                        }
                        if (AllOrders is null)
                        {
                            Orders = new BindingList<Order>(Orders.Where(el => el.IsInRequest).ToList());
                            Orders.AllowEdit = true;
                            Orders.AllowNew = true;
                            Orders.AllowRemove = true;
                            Orders.RaiseListChangedEvents = true;
                        }
                        else 
                        {
                            Orders = new BindingList<Order>(AllOrders.Where(el => el.IsInRequest).ToList());
                            Orders.AllowEdit = true;
                            Orders.AllowNew = true;
                            Orders.AllowRemove = true;
                            Orders.RaiseListChangedEvents = true;
                        }
                    }));
            }
        }
        private RelayCommand _adNewdOrderCommand;
        public RelayCommand AdNewdOrderCommand
        {
            get
            {
                return _adNewdOrderCommand ??
                    (_adNewdOrderCommand = new RelayCommand(obj =>
                    {
                        if (SelectedIndex != 0)
                        {
                            AllOrdersFunc();
                            SaveOrders(1);
                        }
                        Window1 addOrder = new Window1(this);
                        addOrder.Show();
                    }));
            }
        }
        private RelayCommand _overdueCommand;
        public RelayCommand OverdueCommand
        {
            get
            {
                return _overdueCommand ??
                    (_overdueCommand = new RelayCommand(obj =>
                    {
                        SelectedIndex = 4;
                        if (AllOrders is null)
                        {
                            AllOrders = Orders;
                            AllOrders.AllowEdit = true;
                            AllOrders.AllowNew = true;
                            AllOrders.AllowRemove = true;
                            AllOrders.RaiseListChangedEvents = true;
                        }
                        if (AllOrders is null)
                        {
                            Orders = new BindingList<Order>(Orders.Where(el => !el.IsFinished && el.Date_end < DateTime.Now).ToList());
                            Orders.AllowEdit = true;
                            Orders.AllowNew = true;
                            Orders.AllowRemove = true;
                            Orders.RaiseListChangedEvents = true;
                        }
                        else 
                        {
                            Orders = new BindingList<Order>(AllOrders.Where(el => !el.IsFinished && el.Date_end < DateTime.Now).ToList());
                            Orders.AllowEdit = true;
                            Orders.AllowNew = true;
                            Orders.AllowRemove = true;
                            Orders.RaiseListChangedEvents = true;
                        }
                    }));
            }
        }
       private int _selectedIndexOrder;
        public int SelectedIndexOrder { get => _selectedIndexOrder; set => Set(ref _selectedIndexOrder, value); }
        private RelayCommand _onRequesteCommand;
        public RelayCommand OnRequesteCommand
        {
            get
            {
                return _onRequesteCommand ??
                    (_onRequesteCommand = new RelayCommand(obj =>
                    {
                       if(SelectedIndexOrder >-1)
                           Orders[SelectedIndexOrder].IsInRequest = true;
                    }));
            }
        }
        private RelayCommand _selectionChanged;
        private List<Order> _selectedItems;
        private int _selectedIndexTypeOrder=-1;
        public int SelectedIndexTypeOrder { get => _selectedIndexTypeOrder; set => Set(ref _selectedIndexTypeOrder, value); }
        public RelayCommand SelectionChengedCommand
        {
            get
            {
                return _selectionChanged ??
                    (_selectionChanged = new RelayCommand(obj =>
                    {
                        System.Collections.IList items = (System.Collections.IList)obj;
                        _selectedItems = items.Cast<Order>().ToList();
                        if(_selectedItems.Count(it=>it.TypeOrder== _selectedItems[0].TypeOrder) == _selectedItems.Count && _selectedItems.Count!=0)
                        {
                            SelectedIndexTypeOrder = TypeOrders.IndexOf(_selectedItems[0].TypeOrder);
                        }
                        if(_selectedItems.Count == 0)
                        {
                            SelectedIndexTypeOrder = -1;
                        }
                    }));
            }
        }
        private int _selectedIndexAllOrder;
        public int SelectedIndexAllOrder { get => _selectedIndexAllOrder; set => Set(ref _selectedIndexAllOrder, value); }
        private RelayCommand _rightClickCommand;
        public RelayCommand RightClickCommand
        {
            get
            {
                return _rightClickCommand ??
                    (_rightClickCommand = new RelayCommand(obj =>
                    {
                        Order item = (Order)obj;                        
                        SelectedIndexOrder = Orders.IndexOf(item);
                        if (AllOrders != null)
                        {
                            SelectedIndexAllOrder = AllOrders.IndexOf(item);
                        }
                    }));
            }
        }
        private RelayCommand _changeColourCommand;
        public RelayCommand ChangeColourCommand
        {
            get
            {
                    return _changeColourCommand ??
                        (_changeColourCommand = new RelayCommand(obj =>
                        {
                            if (!IsReadOnly)
                            {
                                TypeOrder typeOrder = (TypeOrder)obj;
                                System.Windows.Forms.ColorDialog colorPicker = new System.Windows.Forms.ColorDialog();
                                System.Windows.Media.Color firstColor = Colors.White;
                                System.Windows.Media.Color secondColor = Colors.White;
                                if (colorPicker.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                {
                                    firstColor = System.Windows.Media.Color.FromArgb(colorPicker.Color.A, colorPicker.Color.R, colorPicker.Color.G, colorPicker.Color.B);
                                }
                                MessageBox.Show("Выберите второй цвет!");
                                if (colorPicker.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                {
                                    secondColor = System.Windows.Media.Color.FromArgb(colorPicker.Color.A, colorPicker.Color.R, colorPicker.Color.G, colorPicker.Color.B);
                                }
                                RadialGradientBrush radialGradientBrush = new RadialGradientBrush();
                                radialGradientBrush.GradientStops.Add(new GradientStop(firstColor, 0.0));
                                radialGradientBrush.GradientStops.Add(new GradientStop(secondColor, 1));
                                typeOrder.BackgroundOrder = radialGradientBrush;
                            }
                        }));
            }
        }
        private RelayCommand _onCompleetCommand;
        public RelayCommand OnCompleetCommand
        {
            get
            {
                return _onCompleetCommand ??
                    (_onCompleetCommand = new RelayCommand(obj =>
                    {
                        if (SelectedIndexOrder > -1)
                        {
                            Orders[SelectedIndexOrder].IsFinished = true;
                            Orders[SelectedIndexOrder].NameCancel = FIO;
                        }
                    }));
            }
        }
        private RelayCommand _onCompleetCancellationCommand;
        public RelayCommand OnCompleetCancellationCommand
        {
            get
            {
                return _onCompleetCancellationCommand ??
                    (_onCompleetCancellationCommand = new RelayCommand(obj =>
                    {
                        if (SelectedIndexOrder > -1)
                        {
                            Orders[SelectedIndexOrder].IsFinished = false;
                            Orders[SelectedIndexOrder].NameCancel = FIO;
                        }
                    }));
            }
        }

        private RelayCommand _onDeleteCommand;
        public RelayCommand OnDeleteCommand
        {
            get
            {
                return _onDeleteCommand ??
                    (_onDeleteCommand = new RelayCommand(obj =>
                    {
                        if (SelectedIndexOrder > -1)
                            Orders.RemoveAt(SelectedIndexOrder);
                        if (SelectedIndexAllOrder > -1)
                        {
                            AllOrders.RemoveAt(SelectedIndexAllOrder);
                        }
                        changed = true;
                    }));
            }
        }

        private RelayCommand _onCompleetOrdersCommand;
        public RelayCommand OnCompleetOrdersCommand
        {
            get
            {
                return _onCompleetOrdersCommand ??
                    (_onCompleetOrdersCommand = new RelayCommand(obj =>
                    {
                        System.Collections.IList items = (System.Collections.IList)obj;
                        var listOrders = items.Cast<Order>().ToList();
                        if (listOrders.Count > 0 && listOrders != null)
                            foreach (var it in listOrders)
                            {
                                Orders[Orders.IndexOf(it)].IsFinished = true;
                                Orders[Orders.IndexOf(it)].NameCancel = FIO;
                            }
                    }));
            }
        }

        private RelayCommand _onCompleetCancellationOrdersCommand;
        public RelayCommand OnCompleetCancellationOrdersCommand
        {
            get
            {
                return _onCompleetCancellationOrdersCommand ??
                    (_onCompleetCancellationOrdersCommand = new RelayCommand(obj =>
                    {
                        System.Collections.IList items = (System.Collections.IList)obj;
                        var listOrders = items.Cast<Order>().ToList();
                        if (listOrders.Count > 0 && listOrders != null)
                            foreach (var it in listOrders)
                            {
                                Orders[Orders.IndexOf(it)].IsFinished = false;
                                Orders[Orders.IndexOf(it)].NameCancel = FIO;
                            }
                    }));
            }
        }
        private RelayCommand _onDeleteOrdersCommand;
        public RelayCommand OnDeleteOrdersCommand
        {
            get
            {
                return _onDeleteOrdersCommand ??
                    (_onDeleteOrdersCommand = new RelayCommand(obj =>
                    {
                        System.Collections.IList items = (System.Collections.IList)obj;
                        var listOrders = items.Cast<Order>().ToList();
                        if (listOrders.Count > 0 && listOrders != null)
                        {
                            foreach (var it in listOrders)
                            {
                                Orders.RemoveAt(Orders.IndexOf(it));
                            }
                            if (AllOrders != null)
                            {
                                foreach (var it in listOrders)
                                {                                
                                  AllOrders.RemoveAt(AllOrders.IndexOf(it));
                                }
                            }
                        }
                        changed = true;
                    }));
            }
        }

        private RelayCommand _onRequesteOrdersCommand;
        public RelayCommand OnRequesteOrdersCommand
        {
            get
            {
                return _onRequesteOrdersCommand ??
                    (_onRequesteOrdersCommand = new RelayCommand(obj =>
                    {
                        System.Collections.IList items = (System.Collections.IList)obj;
                        var listOrders = items.Cast<Order>().ToList();
                        if (listOrders.Count > 0 && listOrders != null)
                            foreach (var it in listOrders)
                            {
                                Orders[Orders.IndexOf(it)].IsInRequest = true;
                            }
                    }));
            }
        }

        private int _selectedIndex;
        public int SelectedIndex { get => _selectedIndex; set => Set(ref _selectedIndex, value); }
        
    }
}
