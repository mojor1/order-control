﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrdersControl
{
    /// <summary>
    /// Логика взаимодействия для Note.xaml
    /// </summary>
    [ContentProperty("IsReadOnly")]
    public partial class OrderXaml : UserControl
    {
        public bool IsReadOnly
        {
            get => (bool)this.GetValue(IsReadOnlyProperty);
            set => SetValue(IsReadOnlyProperty, value);
        }
        private static readonly DependencyProperty IsReadOnlyProperty
            = DependencyProperty.RegisterAttached(
                nameof(IsReadOnly),
                typeof(bool),
                typeof(OrderXaml),
                new(null));
        public OrderXaml()
        {
            InitializeComponent();
            
        }
    }
}
