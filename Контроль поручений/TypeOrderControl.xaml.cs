﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OrdersControl
{
    /// <summary>
    /// Логика взаимодействия для TypeOrderControl.xaml
    /// </summary>
    [ContentProperty("IsReadOnly")]
    public partial class TypeOrderControl : UserControl
    {
        public bool IsReadOnly
        {
            get => (bool)this.GetValue(IsReadOnlyProperty);
            set => SetValue(IsReadOnlyProperty, value);
        }
        private static readonly DependencyProperty IsReadOnlyProperty
            = DependencyProperty.RegisterAttached(
                nameof(IsReadOnly),
                typeof(bool),
                typeof(TypeOrderControl),
                new(null));
        public TypeOrderControl()
        {
            InitializeComponent();
        }
    }
}
