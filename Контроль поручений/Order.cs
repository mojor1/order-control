﻿using System;
using System.Text.Json.Serialization;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using OrdersControl;

namespace OrdersControl
{
    public class Order: BaseInpc
    {
        private DateTime _startTime;
        private DateTime _date_end;
        private string _numbDoc;
        private string _noteS;
        private bool _isFinished;
        private bool _isInRequest;
        private string _performerName;
        private int _iD;
        private string _nameCancel;
        private TypeOrder _typeOrder;
        public DateTime Date_start 
        { 
            get => _startTime;
            set => Set(ref _startTime, value);              
        }
        public DateTime Date_end
        {
            get => _date_end;
            set => Set(ref _date_end, value);
        }
        public string NumbDoc 
        {
            get => _numbDoc;
            set => Set(ref _numbDoc, value);
        }
        
        public string NoteS 
        {
            get => _noteS;
            set => Set(ref _noteS, value);
        }
        public bool IsFinished
        {
            get => _isFinished;
            set => Set(ref _isFinished, value);
        }
        public bool IsInRequest
        {
            get => _isInRequest;
            set => Set(ref _isInRequest, value);
        }
        public string PerformerName
        {
            get => _performerName;
            set => Set(ref _performerName, value);
        }
        public int ID 
        {
            get => _iD;
            set => Set(ref _iD, value);
        }
        public string NameCancel 
        {
            get => _nameCancel;
            set => Set(ref _nameCancel, value);
        }
        [JsonInclude]
        public TypeOrder TypeOrder
        {
            get => _typeOrder;
            set => Set(ref _typeOrder, value);
        }

        [JsonConstructor]
        public Order(DateTime date_start, DateTime date_end, string noteS, TypeOrder typeOrder, int id, bool isFinished, string performerName, string numbDoc, string nameCancel, bool isInRequest)
        {
            Date_start = date_start;
            Date_end = date_end;
            NoteS = noteS;
            TypeOrder = typeOrder;
            IsFinished = isFinished;
            ID = id;
            PerformerName = performerName;
            NumbDoc = numbDoc;
            NameCancel = nameCancel;
            IsInRequest = isInRequest;
        }
        public Order(DateTime date_end, string noteS, TypeOrder typeOrder, int id, string performerName, string numbDoc)
        {
            Date_start = DateTime.Now.Date;
            Date_end = date_end;
            NoteS = noteS;
            TypeOrder = typeOrder;
            IsFinished = false;
            ID = id;
            PerformerName = performerName;
            NumbDoc = numbDoc;
        }

        public Order()
        {
        }

        public Order(TypeOrder typeOrder)
        {
            TypeOrder = typeOrder;
            Date_start = DateTime.Now;
            Date_end = DateTime.Now;
        }

        public bool Proverka(int day)
        {
            if ((Date_end - DateTime.Now.Date).Days <= day)
                return true;
            else return false;
        }
    }
}
