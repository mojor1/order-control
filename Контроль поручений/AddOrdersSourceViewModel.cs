﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace OrdersControl
{
    class AddOrdersSourceViewModel : BaseInpc
    {
        private OrdersSourceViewModel _mainWindow;

        public AddOrdersSourceViewModel(OrdersSourceViewModel mainWindow)
        {
            MainWindow = mainWindow;
        }
        private TypeOrder? _selectedType;
        public TypeOrder? SelectedType { get => _selectedType; set => Set(ref _selectedType, value); }
        public OrdersSourceViewModel MainWindow { get => _mainWindow; set => Set(ref _mainWindow, value); }
        private RelayCommand _addOrderCommand;
        public RelayCommand AddOrdersCommand
        {
            get
            {
                return _addOrderCommand ??
                    (_addOrderCommand = new RelayCommand(obj =>
                    {
                        Order newOrder = (Order)obj;
                        MainWindow.Orders.Add(newOrder);
                        if (MessageBox.Show("Хотите добавить ещё одно поручение?",
"Поручения", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        {
                            foreach (Window it in App.Current.Windows)
                            {
                                if (it.Name == "WindowAddOrder")
                                {
                                    it.Close();
                                    break;
                                }
                            }
                        }                       
                    })
                    {

                    });
            }
        }
        private RelayCommand _selectionChanged;
        public RelayCommand SelectionChengedCommand
        {
            get
            {
                return _selectionChanged ??
                    (_selectionChanged = new RelayCommand(obj =>
                    {
                        Order order = (Order)obj;
                        order.TypeOrder = SelectedType;
                    }));
            }
        }

    }
}
