﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OrdersControl
{
    public class SizeConvertor : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double whidth = System.Convert.ToDouble(value) / 5;
            return whidth;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
    public class VisibilityConvertor : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isvisible = System.Convert.ToBoolean(value);
            if (isvisible)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
    public class AntiVisibilityConvertor : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isvisible = System.Convert.ToBoolean(value);
            if (isvisible)
            {
                return Visibility.Collapsed;
            }
            else
            {
                return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
    public class HeightConvertor : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double hight = System.Convert.ToDouble(value) *0.115;
            return hight;
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
    public class ImageConvertor : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                bool IsFinished = System.Convert.ToBoolean(values[0]);
                DateTime Date_end = System.Convert.ToDateTime(values[1]);
                bool IsInRequst = System.Convert.ToBoolean(values[2]);
                if (IsFinished)
                {
                    Uri resourceUri = new Uri("completed.png", UriKind.Relative);
                    return new BitmapImage(resourceUri);
                }
                else if (IsInRequst)
                {
                    Uri resourceUri = new Uri("letter.png", UriKind.Relative);
                    return new BitmapImage(resourceUri);
                }
                else if (Date_end < DateTime.Now)
                {
                    Uri resourceUri = new Uri("overdue.png", UriKind.Relative);
                    return new BitmapImage(resourceUri);
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public object[] ConvertBack(object value, System.Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
    public class BorderConvertor : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                bool IsFinished = System.Convert.ToBoolean(values[0]);
                DateTime Date_end = System.Convert.ToDateTime(values[1]);
                int? CountDay = System.Convert.ToInt32(values[2]);
                    if (IsFinished)
                    {
                        return Brushes.Green;
                    }
                    else if (Date_end < DateTime.Now)
                    {
                        return Brushes.Red;
                    }
                    else if ((Date_end - DateTime.Now).Days <= CountDay)
                    {
                        return Brushes.Orange;
                    }
                    else
                    {
                        return new SolidColorBrush(Color.FromArgb(255, 234, 213, 24));
                    }
            }
            catch
            {
                return null;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    public class MultiVisibilityTrueConverter: IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                bool IsReadOnly = System.Convert.ToBoolean(values[0]);
                int SelectedIndex = System.Convert.ToInt32(values[1]);
                if (IsReadOnly && SelectedIndex>-1)
                {
                    return Visibility.Visible;
                }
                else 
                {
                    return Visibility.Collapsed;
                }               
            }
            catch
            {
                return Visibility.Collapsed;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    public class MultiVisibilityFalseConverter : IMultiValueConverter
    {
        public object Convert(object[] values, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                bool IsReadOnly = System.Convert.ToBoolean(values[0]);
                int SelectedIndex = System.Convert.ToInt32(values[1]);
                if (!IsReadOnly && SelectedIndex > -1)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            catch
            {
                return Visibility.Collapsed;
            }
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
