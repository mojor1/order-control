﻿using OrdersControl;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows.Media;

namespace OrdersControl
{
    public class TypeOrder : BaseInpc
    {
        private int _countDay;
        private byte _firstA;
        private byte _firstR;
        private byte _firstG;
        private byte _firstB;
        private byte _secondA;
        private byte _secondR;
        private byte _secondG;
        private byte _secondB;
        private RadialGradientBrush _backgroundOrder;

        public TypeOrder()
        {
        }
        
        [JsonConstructor]
        public TypeOrder(int countDay, byte firstA, byte firstR, byte firstG, byte firstB, byte secondA, byte secondR, byte secondG, byte secondB)
        {
            
            CountDay = countDay;
           var backgroundOrder = new RadialGradientBrush();
            backgroundOrder.GradientStops.Add(new GradientStop(System.Windows.Media.Color.FromArgb(firstA, firstR, firstG, firstB), 0.0));
            backgroundOrder.GradientStops.Add(new GradientStop(System.Windows.Media.Color.FromArgb(secondA, secondR, secondG, secondB), 1));
            BackgroundOrder = backgroundOrder;
        }
        public TypeOrder(int countDay, RadialGradientBrush backgroundOrder)
        {
            CountDay = countDay;
            BackgroundOrder = backgroundOrder;
        }
        public static bool operator ==(TypeOrder typeOrder1, TypeOrder typeOrder2)
        {
            if (typeOrder2 is null || typeOrder1 is null)
                return false;
            if (typeOrder1.BackgroundOrder.GradientStops[0].Color == typeOrder2.BackgroundOrder.GradientStops[0].Color && typeOrder1.BackgroundOrder.GradientStops[1].Color == typeOrder2.BackgroundOrder.GradientStops[1].Color &&
                typeOrder1.CountDay == typeOrder2.CountDay)
                return true;            
            return false;
        }
        public static bool operator !=(TypeOrder typeOrder1, TypeOrder typeOrder2)
        {
            if (typeOrder1 == typeOrder2)
                return false;
            return true;
        }

        public int CountDay
        {
            get => _countDay;
            set => Set(ref _countDay, value);
        }
        public byte FirstA
        {
            get => _firstA;
            set => Set(ref _firstA, value);
        }
        public byte FirstR
        {
            get => _firstR;
            set => Set(ref _firstR, value);
        }
        public byte FirstG
        {
            get => _firstG;
            set => Set(ref _firstG, value);
        }
        public byte FirstB
        {
            get => _firstB;
            set => Set(ref _firstB, value);
        }
        public byte SecondA
        {
            get => _secondA;
            set => Set(ref _secondA, value);
        }
        public byte SecondR
        {
            get => _secondR;
            set => Set(ref _secondR, value);
        }
        public byte SecondG
        {
            get => _secondG;
            set => Set(ref _secondG, value);
        }
        public byte SecondB
        {
            get => _secondB;
            set => Set(ref _secondB, value);
        }
        [JsonIgnore]
        public RadialGradientBrush BackgroundOrder
        {
            get => _backgroundOrder;
            set
            {                        
                Set(ref _backgroundOrder, value);
                FirstA = BackgroundOrder.GradientStops[0].Color.A;
                FirstR = BackgroundOrder.GradientStops[0].Color.R;
                FirstG = BackgroundOrder.GradientStops[0].Color.G;
                FirstB = BackgroundOrder.GradientStops[0].Color.B;
                SecondA = BackgroundOrder.GradientStops[1].Color.A;
                SecondR = BackgroundOrder.GradientStops[1].Color.R;
                SecondG = BackgroundOrder.GradientStops[1].Color.G;
                SecondB = BackgroundOrder.GradientStops[1].Color.B;
            }
        }
    }
}
